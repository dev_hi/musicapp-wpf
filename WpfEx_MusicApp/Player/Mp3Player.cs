﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using Id3;
using System.Windows.Media.Imaging;
using System.Security.AccessControl;

namespace WpfEx_MusicApp.Player {
    public class Mp3Player : IDisposable {
		public double Duration {
			get { return this._mp3Reader.TotalTime.TotalMilliseconds; }
		}
		public double CurrnetTime {
			get { return this._mp3Reader.CurrentTime.TotalMilliseconds; }
			set { this._mp3Reader.CurrentTime = new TimeSpan((long)(value * 10000)); }
		}
		public Stream MusicStream;
		private string Filename;
		public bool Playing { get { return this._waveOutput.PlaybackState == PlaybackState.Playing; } }
		public bool Paused { get { return this._waveOutput.PlaybackState == PlaybackState.Paused; } }
		public bool Stopped { get { return this._waveOutput.PlaybackState == PlaybackState.Stopped; } }

		private Mp3FileReader _mp3Reader;
		private WaveOut _waveOutput = new WaveOut();
		private Id3Tag _tag;
		private bool StreamClosed = true;

		public string Title {
			get {
				if (this._tag == null) return this.Filename.Substring(this.Filename.LastIndexOf('\\') + 1);
				return this._tag.Title.Value;
			}
		}

		public string Artist {
			get {
				if (this._tag == null) return "[정보 없음]";
				return this._tag.Artists.Value[0];
			}
		}

		public BitmapImage GetJacket() {
			if (this._tag == null || this._tag.Pictures.Count == 0) return null;
			MemoryStream ms = new MemoryStream(this._tag.Pictures[0].PictureData);
			BitmapImage ret = new BitmapImage();
			ret.BeginInit();
			ret.StreamSource = ms;
			ret.EndInit();
			return ret;
		}

		public Mp3Player(string filename) {
			this.Filename = filename;
			this.NewStream();
		}
		

		public void Play() {
			try {
				if (this.StreamClosed)
					this.NewStream();
				if (!this.StreamClosed)
					this._waveOutput.Play();
			}
			catch(Exception) {
				try {
					this._waveOutput.Pause();
				}
				catch(Exception) { }
			}
		}

		public void SetVolume(int a) {
			if (a > 100) this._waveOutput.Volume = 1;
			else if (a <= 0) this._waveOutput.Volume = 0;
			else this._waveOutput.Volume = ((float)100.0 / a);
			
		}
		
		public int GetVolume() {
			return (int)(this._waveOutput.Volume * 100);
		}

		public void Seek(double mils) {
			this.CurrnetTime = mils;
		}

		public void Pause() {
			try {
				if(!this.StreamClosed) 
					this._waveOutput.Pause();
			}
			catch (Exception) { }
		}

		public void Stop() {
			try {
				this._waveOutput.Stop();
				this.CloseStream();
			}
			catch (Exception) { }
		}

		public void TogglePause() {
			if(this.Playing) {
				this.Pause();
			}
			else if(!this.Playing) {
				this.Play();
			}
		}

		private void CloseStream() {
			if(!this.StreamClosed) {
				if (this._waveOutput != null) this._waveOutput.Dispose();
				this.MusicStream.Close();
				this.StreamClosed = true;
			}
		}

		private void NewStream() {
			if(this.StreamClosed) {
				this.MusicStream = new FileStream(this.Filename, FileMode.Open, FileSystemRights.Read, FileShare.ReadWrite, 4096, FileOptions.None);
				this._mp3Reader = new Mp3FileReader(this.MusicStream);
				this._waveOutput.Init(this._mp3Reader);
				Mp3 mp3 = new Mp3(this.MusicStream);
				this._tag = mp3.GetTag(Id3Version.V23);
				StreamClosed = false;
			}
		}

		#region IDisposable Support
		private bool disposedValue = false; // 중복 호출 차단

		protected virtual void Dispose(bool disposing) {
			if (!disposedValue) {
				if (disposing) {
					this.CloseStream();
				}

				// TODO: 관리되지 않는 리소스(관리되지 않는 개체)를 해제하고 아래의 종료자를 재정의합니다.
				// TODO: 큰 필드를 null로 설정합니다.

				disposedValue = true;
			}
		}

		// TODO: 위의 Dispose(bool disposing)에 관리되지 않는 리소스를 해제하는 코드가 포함되어 있는 경우에만 종료자를 재정의합니다.
		// 삭제 가능한 패턴을 올바르게 구현하기 위해 추가된 코드입니다.

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}
