﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfEx_MusicApp.Player;
using WpfEx_MusicApp.Controls;

namespace WpfEx_MusicApp {
	/// <summary>
	/// MainWindow.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class MainWindow : Window {

		public MainWindow() {
			InitializeComponent();

		}


		#region WndCaption
		bool WindowWillMove = false;
		private void ToggleMaximize() {
			if (this.WindowMaximizeButton.IsEnabled) {
				if (this.WindowState == WindowState.Maximized) {
					this.WindowState = WindowState.Normal;
					((Image)this.WindowMaximizeButton.Content).Source = (BitmapImage)this.Resources["MaxButtonImage"];
				}
				else if (this.WindowState == WindowState.Normal) {
					this.WindowState = WindowState.Maximized;
					((Image)this.WindowMaximizeButton.Content).Source = (BitmapImage)this.Resources["NorButtonImage"];
				}
			}
		}
		private void WindowCloseButton_Click(object sender, RoutedEventArgs e) {
			this.Close();
		}
		private void WindowMaximizeButton_Click(object sender, RoutedEventArgs e) {
			ToggleMaximize();
		}
		private void WindowMinimizeButton_Click(object sender, RoutedEventArgs e) {
			WindowState = WindowState.Minimized;
		}
		private void ControlArea_MouseDown(object sender, MouseButtonEventArgs e) {
			if (e.ClickCount == 2) {
				ToggleMaximize();
			}
			if (e.ChangedButton == MouseButton.Left) {
				WindowWillMove = true;
				this.DragMove();
			}
		}
		private void ControlArea_MouseMove(object sender, MouseEventArgs e) {
			if (WindowWillMove && e.LeftButton == MouseButtonState.Pressed && (this.WindowState == WindowState.Maximized)) {
				ToggleMaximize();
				Point t = e.GetPosition(this);
				this.Left = t.X - this.Left;
				this.Top = t.Y - 20;
				this.DragMove();
			}
		}
		private void ControlArea_MouseUp(object sender, MouseButtonEventArgs e) {
			WindowWillMove = false;
		}
		#endregion

		private Mp3Player _currentMusic;


		private void TogglePause() {
			if (this._currentMusic != null) {
				if (this._currentMusic.Playing) {
					this._currentMusic.Pause();
					this.PlayButton.Visibility = Visibility.Visible;
					this.PauseButton.Visibility = Visibility.Collapsed;
				}
				else {
					this._currentMusic.Play();
					this.PlayButton.Visibility = Visibility.Collapsed;
					this.PauseButton.Visibility = Visibility.Visible;
				}
			}
		}

		private void CurrrentMusicSlider_OnSeek(object sender, RoutedSeekEventArgs e) {
			this._currentMusic.CurrnetTime = e.SeekTime;
		}

		private void MusicListControl_ItemDoubleClick(object sender, RoutedItemDoubleClickEventArgs e) {
			this.ChangeMusic(this.MusicListControl.Select(e.TargetPlayer));
			this.TogglePause();
		}

		private void ChangeMusic(Mp3Player m) {
			if (this._currentMusic != null) {
				this._currentMusic.Stop();
				this._currentMusic.Dispose();
			}
			this._currentMusic = m;
			this.CurrrentMusicSlider.ChangeMusic(this._currentMusic);
			this.TitleLabel.Content = this._currentMusic.Title;
			this.ArtistLabel.Content = this._currentMusic.Artist;
			BitmapImage img = this._currentMusic.GetJacket();
			if (img != null) {
				ChangeSource(this.image_test, img, new TimeSpan(5000000), new TimeSpan(5000000)); //페이드효과 있는거
				ChangeSource(this.image_test2, img, new TimeSpan(5000000), new TimeSpan(5000000)); //페이드효과 있는거
																								   //this.Resources["CurrentPlayingJacket"] = img; //효과 없는거
			}
		}

		public static void ChangeSource(Image image, ImageSource source, TimeSpan fadeOutTime, TimeSpan fadeInTime) {
			var fadeInAnimation = new DoubleAnimation(1d, fadeInTime);

			if (image.Source != null) {
				var fadeOutAnimation = new DoubleAnimation(0d, fadeOutTime);

				fadeOutAnimation.Completed += (o, e) => {
					image.Source = source;
					image.BeginAnimation(Image.OpacityProperty, fadeInAnimation);
				};

				image.BeginAnimation(Image.OpacityProperty, fadeOutAnimation);
			}
			else {
				image.Opacity = 0d;
				image.Source = source;
				image.BeginAnimation(Image.OpacityProperty, fadeInAnimation);
			}
		}

		private void PlayButton_Click(object sender, RoutedEventArgs e) {
			if (this._currentMusic == null && this.MusicListControl.ListCount > 0) {
				Mp3Player p = this.MusicListControl.SelectNext(true);
				if(p != null) this.ChangeMusic(p);
			}
			TogglePause();
		}

		private void PrevButton_Click(object sender, RoutedEventArgs e) {
			Mp3Player m;
			if ((m = this.MusicListControl.SelectPrev(true)) == null)
				return;
			this.ChangeMusic(m);
			TogglePause();
		}

		private void NextButton_Click(object sender, RoutedEventArgs e) {
			Mp3Player m;
			if ((m = this.MusicListControl.SelectNext(true)) == null)
				return;
			this.ChangeMusic(m);
			TogglePause();
		}

		private void PauseButton_Click(object sender, RoutedEventArgs e) {
			TogglePause();
		}
	}
}
