﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfEx_MusicApp.Controls
{
	public class ColAutoWidth : DependencyObject {
		public static readonly DependencyProperty AutoWidthProperty = DependencyProperty.RegisterAttached("AutoWidth", typeof(bool), typeof(ColAutoWidth), new UIPropertyMetadata(false));

		public static bool GetAutoWidth(DependencyObject obj) {
			return (bool)obj.GetValue(AutoWidthProperty);
		}

		public static void SetAutoWidth(DependencyObject obj, bool value) {
			obj.SetValue(AutoWidthProperty, value);
		}

		public static void AutoWidthAdjust(ListView listview) {
			if (listview.View.GetType() == typeof(GridView)) {
				/*
				 AutoWidth가 아닌 컬럼들의 길이 총합 (절대길이, total absolute width)를
				 ListView의 실제 길이에서 뺀다음 남은 길이를 사용가능한 "총 상대길이"(total relative width)라고 한다
				 그렇게 해서 구한 "총 상대길이"를 100%로 보고, 
				 AutoWidth가 참인 컬럼들의 비율이 "총 상대길이" 내에서 차지하는 비율을 구해서 총 상대길이에 곱하면 컬럼 자신의 새 길이를 구할 수 있다.
				 (WPF의 그리드 시스템을 보고 그대로 구현한것.)
				 */

				GridView view = (GridView)listview.View;
				double width_total = listview.ActualWidth;
				double width_absolutes = 0; //AutoWidth가 아닌 열들의 길이 총합 (절대길이)

				List<GridViewColumn> columns_relative = new List<GridViewColumn>(); //길이를 수정해야하는 컬럼 리스트
				double width_min = double.MaxValue; //상대길이의 기준길이로 사용하기 위해 가장 작은 길이를 저장
				for (int i = 0; i < view.Columns.Count; ++i) {
					if ((bool)view.Columns[i].GetValue(AutoWidthProperty)) {
						columns_relative.Add(view.Columns[i]);
						width_min = Math.Min(width_min, view.Columns[i].ActualWidth);
					}
					else {
						width_absolutes += view.Columns[i].ActualWidth;
					}
				}
				List<double> weights = new List<double>(); //relative 컬럼의 상대배율 (기준 길이를 1로 보았을때 자신의 길이의 상대적 배율) 리스트
				double total_weights = 0; //배율 총합
				for (int i = 0; i < columns_relative.Count; ++i) {
					double w = columns_relative[i].ActualWidth / width_min;
					weights.Add(w);
					total_weights += w;
				}

				double width_relatives = (width_total - width_absolutes);

				for (int i = 0; i < columns_relative.Count; ++i) {
					columns_relative[i].Width = width_relatives * (weights[i] / total_weights);
					/* 
					 * 컬럼의 상대배율을 배율의 총합으로 나누면 자신의 비율이 나옴 (예 - 0.7, 0.2, 0.1 (무조건 총합 1.0, 100%))
					 * 이 값을 width_relatives에 곱하면 실제 길이가 나옴
					 */

				}
			}
		}
	}
}
