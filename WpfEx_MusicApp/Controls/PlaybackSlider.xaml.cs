﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using WpfEx_MusicApp.Player;
using System.Timers;
using System.Threading;

namespace WpfEx_MusicApp.Controls {
	/// <summary>
	/// PlaybackSlider.xaml에 대한 상호 작용 논리
	/// </summary>
	/// 

	public class RoutedSeekEventArgs : RoutedEventArgs {
		public double SeekTime { get; private set; }
		public RoutedSeekEventArgs(RoutedEvent e, object sender, double data) : base(e, sender) {
			this.SeekTime = data;
		}
	}
	public delegate void OnSeekEventHandler(object sender, RoutedSeekEventArgs e);

	public partial class PlaybackSlider : UserControl {
		public static readonly RoutedEvent SeekEvent = EventManager.RegisterRoutedEvent("OnSeek", RoutingStrategy.Bubble, typeof(OnSeekEventHandler), typeof(PlaybackSlider));

		public event OnSeekEventHandler OnSeek {
			add { AddHandler(SeekEvent, value); }
			remove { RemoveHandler(SeekEvent, value); }
		}
		private Mp3Player _currentMusic;
		private bool _userActive = false;
		private System.Timers.Timer _timer = new System.Timers.Timer(1000);

		public PlaybackSlider() {
			this._timer.Elapsed += this.Timer_Elapsed;
			this._timer.Start();
			InitializeComponent();
		}

		public void ChangeMusic(Mp3Player m) {
			this._currentMusic = m;
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
			try {
				this.TargetSlider.Dispatcher.Invoke(new ElapsedEventHandler(InvokedEventHandler), new object[] { sender, e });
			} catch (Exception) { }
		}

		private void InvokedEventHandler(object sender, ElapsedEventArgs e) {
			if (this._currentMusic != null && !this._userActive) 
				this.TargetSlider.Value = this._currentMusic.CurrnetTime / this._currentMusic.Duration * 100;
		}

		private void TargetSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			if (this._currentMusic != null && this._userActive) {
				RoutedSeekEventArgs e2 = new RoutedSeekEventArgs(SeekEvent, sender, this._currentMusic.Duration * (e.NewValue / 100));
				this.RaiseEvent(e2);
			}
		}
		private void TargetSlider_MouseUp(object sender, MouseButtonEventArgs e) {
			this._userActive = false;
		}

		private void TargetSlider_MouseDown(object sender, MouseButtonEventArgs e) {
			this._userActive = true;
		}

		private void TargetSlider_DragCompleted(object sender, DragCompletedEventArgs e) {
			this._userActive = false;
		}

		private void TargetSlider_DragStarted(object sender, DragStartedEventArgs e) {
			this._userActive = true;
		}
	}
}
