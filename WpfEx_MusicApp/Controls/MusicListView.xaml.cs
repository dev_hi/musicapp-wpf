﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfEx_MusicApp.Player;

namespace WpfEx_MusicApp.Controls {
	/// <summary>
	/// MusicListView.xaml에 대한 상호 작용 논리
	/// </summary>
	public enum PlaybackOrderMode {
		None, RepeatList, RepeatCurrent, Random
	}
	public class RoutedItemDoubleClickEventArgs : RoutedEventArgs {
		public Mp3Player TargetPlayer { get; private set; }
		public RoutedItemDoubleClickEventArgs(RoutedEvent e, object sender, Mp3Player data) : base(e, sender) {
			this.TargetPlayer = data;
		}
	}
	public delegate void ItemDoubleClickEventHandler(object sender, RoutedItemDoubleClickEventArgs e);

	public partial class MusicListView : UserControl {

		public static readonly Color DefaultBackground = Colors.Transparent;
		public static readonly Color DefaultForeground = Colors.White;
		public static readonly Color DefaultItemFocusBackground = Color.FromArgb(0x50, 0xFF, 0xFF, 0xFF);
		public static readonly Color DefaultItemFocusForeground = Colors.White;
		public static readonly Color DefaultItemSelectedBackground = Color.FromArgb(0x70, 0xFF, 0xFF, 0xFF);
		public static readonly Color DefaultItemSelectedForeground = Colors.White;
		public static readonly Color DefaultItemSelectedFocusBackground = Color.FromArgb(0x90, 0xFF, 0xFF, 0xFF);
		public static readonly Color DefaultItemSelectedFocusForeground = Colors.White;

		public new static readonly DependencyProperty BackgroundProperty =
			DependencyProperty.Register("Background", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultBackground), new PropertyChangedCallback(PropertiesChanged)));
		public new static readonly DependencyProperty ForegroundProperty =
			DependencyProperty.Register("Foreground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultForeground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty ItemFocusBackgroundProperty =
			DependencyProperty.Register("ItemFocusBackground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultItemFocusBackground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty ItemFocusForegroundProperty =
			DependencyProperty.Register("ItemFocusForeground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultItemFocusForeground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty ItemSelectedBackgroundProperty =
			DependencyProperty.Register("ItemSelectedBackground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultItemSelectedBackground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty ItemSelectedForegroundProperty =
			DependencyProperty.Register("ItemSelectedForeground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultItemSelectedForeground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty ItemSelectedFocusBackgroundProperty =
			DependencyProperty.Register("ItemSelectedFocusBackground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultItemSelectedFocusBackground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty ItemSelectedFocusForegroundProperty =
			DependencyProperty.Register("ItemSelectedFocusForeground", typeof(Brush), typeof(MusicListView), new FrameworkPropertyMetadata(new SolidColorBrush(DefaultItemSelectedFocusForeground), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly RoutedEvent ItemDoubleClickEvent =
			EventManager.RegisterRoutedEvent("ItemDoubleClick", RoutingStrategy.Bubble, typeof(ItemDoubleClickEventHandler), typeof(MusicListView));
		public static readonly DependencyProperty ListOrderModeProperty =
			DependencyProperty.RegisterAttached("ListOrderMode", typeof(PlaybackOrderMode), typeof(MusicListView), new FrameworkPropertyMetadata(PlaybackOrderMode.RepeatList, new PropertyChangedCallback(PropertiesChanged)));

		public new Brush Background {
			get { return ((Brush)this.Resources["Button.Background"]); }
			set { this.Resources["Button.Background"] = value; }
		}
		public new Brush Foreground {
			get { return ((Brush)this.Resources["Foreground"]); }
			set { this.Resources["Foreground"] = value; }
		}
		public Brush ItemFocusBackground {
			get { return ((Brush)this.Resources["Item.Focus.Background"]); }
			set { this.Resources["Item.Focus.Background"] = value; }
		}
		public Brush ItemFocusForeground {
			get { return ((Brush)this.Resources["Item.Focus.Foreground"]); }
			set { this.Resources["Item.Focus.Foreground"] = value; }
		}
		public Brush ItemSelectedBackground {
			get { return ((Brush)this.Resources["Item.Selected.Background"]); }
			set { this.Resources["Item.Selected.Background"] = value; }
		}
		public Brush ItemSelectedForeground {
			get { return ((Brush)this.Resources["Item.Selected.Foreground"]); }
			set { this.Resources["Item.Selected.Foreground"] = value; }
		}
		public Brush ItemSelectedFocusBackground {
			get { return ((Brush)this.Resources["Item.Selected.Focus.Background"]); }
			set { this.Resources["Item.Selected.Focus.Background"] = value; }
		}
		public Brush ItemSelectedFocusForeground {
			get { return ((Brush)this.Resources["Item.Selected.Focus.Foreground"]); }
			set { this.Resources["Item.Selected.Focus.Foreground"] = value; }
		}
		public int ListCount {
			get { return this._Playlist.Count; }
		}
		public event ItemDoubleClickEventHandler OnItemDoubleClick {
			add { AddHandler(ItemDoubleClickEvent, value); }
			remove { RemoveHandler(ItemDoubleClickEvent, value); }
		}
		//TODO: 반복모드 구현을 해야할까...? 또 버튼 만들기도 귀찬은데?
		public PlaybackOrderMode ListOrderMode = PlaybackOrderMode.RepeatList;

		private List<Mp3Player> _Playlist = new List<Mp3Player>();
		private bool _FileAdding = false;
		private OpenFileDialog _OpenFileDialog = new OpenFileDialog() { Multiselect = true };
		private delegate void DispatcherInvokeDelegate(bool invoked);

		public MusicListView() {
			InitializeComponent();
			this.TargetListView.ItemsSource = this._Playlist;
		}
		private void Grid_Drop(object sender, DragEventArgs e) {
			string[] fileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);
			this.StartFileLoadingThread(fileList);
		}
		private void AddButton_Click(object sender, RoutedEventArgs e) {
			bool? result = this._OpenFileDialog.ShowDialog().Value;
			if (result.HasValue) {
				if (result.Value) {
					this.StartFileLoadingThread(this._OpenFileDialog.FileNames);
				}
			}
		}
		private void TargetListView_SizeChanged(object sender, SizeChangedEventArgs e) {
			if (e.WidthChanged)
				ColAutoWidth.AutoWidthAdjust((ListView)sender);
		}
		private static void PropertiesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
			MusicListView self = (MusicListView)sender;
			if (e.Property == BackgroundProperty) {
				self.Background = (Brush)e.NewValue;
			}
			else if (e.Property == ForegroundProperty) {
				self.Foreground = (Brush)e.NewValue;
			}
			else if (e.Property == ItemFocusBackgroundProperty) {
				self.ItemFocusBackground = (Brush)e.NewValue;
			}
			else if (e.Property == ItemFocusForegroundProperty) {
				self.ItemFocusForeground = (Brush)e.NewValue;
			}
			else if (e.Property == ItemSelectedBackgroundProperty) {
				self.ItemSelectedBackground = (Brush)e.NewValue;
			}
			else if (e.Property == ItemSelectedForegroundProperty) {
				self.ItemSelectedForeground = (Brush)e.NewValue;
			}
			else if (e.Property == ItemSelectedFocusBackgroundProperty) {
				self.ItemSelectedFocusBackground = (Brush)e.NewValue;
			}
			else if (e.Property == ItemSelectedFocusForegroundProperty) {
				self.ItemSelectedFocusForeground = (Brush)e.NewValue;
			}
			else if (e.Property == ListOrderModeProperty) {
				self.ListOrderMode = (PlaybackOrderMode)e.NewValue;
			}
		}

		public Mp3Player SelectNext(bool force_next) {
			int i = this.TargetListView.SelectedIndex;
			if (this.ListOrderMode == PlaybackOrderMode.Random) {
				i = (new Random()).Next(this.ListCount);
			}
			else if(this.ListOrderMode != PlaybackOrderMode.RepeatCurrent || force_next) {
				++i;
			}
			return this.Select(i);
		}
		public Mp3Player SelectPrev(bool force_prev) {
			int i = this.TargetListView.SelectedIndex;
			if (this.ListOrderMode == PlaybackOrderMode.Random) {
				i = (new Random()).Next(this.ListCount);
			}
			else if (this.ListOrderMode != PlaybackOrderMode.RepeatCurrent || force_prev) {
				--i;
			}
			return this.Select(i);
		}
		public Mp3Player Select(int i) {
			Mp3Player ret = null;
			if (this.ListCount > 0) { 
				if (i < 0) {
					if (this.ListOrderMode == PlaybackOrderMode.RepeatList) i = this.ListCount - 1;
				}
				else if (i >= this.ListCount) {
					if (this.ListOrderMode == PlaybackOrderMode.RepeatList) i = 0;
					else i = -1;
				}
				this.TargetListView.SelectedIndex = i;
				if (i > -1) {
					ret = (Mp3Player)(this.TargetListView.SelectedItem);
				}
			}
			return ret;
		}
		public Mp3Player Select(Mp3Player o) {
			return this.Select(this.IndexOf(o));
		}
		public int IndexOf(Mp3Player o) {
			return this._Playlist.IndexOf(o);
		}


		private void RefreshList(bool invoked) {
			if (invoked) this.TargetListView.Items.Refresh();
			else this.TargetListView.Dispatcher.Invoke(new DispatcherInvokeDelegate(RefreshList), true);
		}
		private void ShowLoader(bool invoked) {
			if (invoked) this.LoaderScreen.Visibility = Visibility.Visible;
			else this.TargetListView.Dispatcher.Invoke(new DispatcherInvokeDelegate(ShowLoader), true);
		}
		private void HideLoader(bool invoked) {
			if (invoked) this.LoaderScreen.Visibility = Visibility.Collapsed;
			else this.TargetListView.Dispatcher.Invoke(new DispatcherInvokeDelegate(HideLoader), true);
		}
		private void RowMouseDoubleClick(object sender, MouseButtonEventArgs e) {
			RoutedItemDoubleClickEventArgs e2 = new RoutedItemDoubleClickEventArgs(ItemDoubleClickEvent, this, (Mp3Player)((ListViewItem)sender).Content);
			this.RaiseEvent(e2);
		}
		
		private void AddSongFromFile(string file) {
			try {
				this._Playlist.Add(new Mp3Player(file));
				this.RefreshList(false);
			}
			catch(IOException e) {
				MessageBox.Show(file + " 파일은 다음의 이유로 열수 없습니다.\n" + e.Message, "파일 에러", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		private void LoadFiles(object arg) {
			if (!this._FileAdding) {
				this._FileAdding = true;
				string[] files = (string[])arg;
				ShowLoader(false);
				for (int i = 0; i < files.Length; ++i) {
					if (files[i].Substring(files[i].LastIndexOf('.')) == ".mp3")
						this.AddSongFromFile(files[i]);
				}
				HideLoader(false);
				this._FileAdding = false;
			}
		}
		private void StartFileLoadingThread(string[] files) {
			Thread thread = new Thread(new ParameterizedThreadStart(LoadFiles));
			thread.Start(files);
		}
	}
}
