﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfEx_MusicApp.Controls {
	/// <summary>
	/// ColoredButton.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class ColoredButton : UserControl {
		public ColoredButton() {
			InitializeComponent();
		}
		
		public new static readonly DependencyProperty BackgroundProperty =
	DependencyProperty.Register("Background", typeof(Color), typeof(ColoredButton), new FrameworkPropertyMetadata(Color.FromArgb(0x00, 0xE1, 0xE1, 0xE1), new PropertyChangedCallback(PropertiesChanged)));
		public new static readonly DependencyProperty ForegroundProperty =
			DependencyProperty.Register("Foreground", typeof(Color), typeof(ColoredButton), new FrameworkPropertyMetadata(Color.FromArgb(0xFF, 0, 0, 0), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty FocusBackgroundProperty =
			DependencyProperty.Register("FocusBackground", typeof(Color), typeof(ColoredButton), new FrameworkPropertyMetadata(Color.FromArgb(0x00, 0xE5, 0xF1, 0xFB), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty FocusForegroundProperty =
			DependencyProperty.Register("FocusForeground", typeof(Color), typeof(ColoredButton), new FrameworkPropertyMetadata(Color.FromArgb(0xFF, 0, 0, 0), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty PressedBackgroundProperty =
			DependencyProperty.Register("PressedBackground", typeof(Color), typeof(ColoredButton), new FrameworkPropertyMetadata(Color.FromArgb(0x00, 0xCC, 0xE4, 0xF7), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty PressedForegroundProperty =
			DependencyProperty.Register("PressedForeground", typeof(Color), typeof(ColoredButton), new FrameworkPropertyMetadata(Color.FromArgb(0xFF, 0, 0, 0), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly DependencyProperty CornerRadiusProperty =
			DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(ColoredButton), new FrameworkPropertyMetadata(new CornerRadius(0), new PropertyChangedCallback(PropertiesChanged)));
		//public static readonly DependencyProperty ClickProperty =
		//DependencyProperty.Register("Click", typeof(RoutedEventHandler), typeof(ColoredButton), new FrameworkPropertyMetadata(default(RoutedEventHandler), new PropertyChangedCallback(PropertiesChanged)));
		public static readonly RoutedEvent ClickEvent =
			EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ColoredButton));
		public new static readonly DependencyProperty ContentProperty =
			DependencyProperty.Register("Content", typeof(object), typeof(ColoredButton), new FrameworkPropertyMetadata("", new PropertyChangedCallback(PropertiesChanged)));
			
		public new Color Background {
			get { return ((SolidColorBrush)this.Resources["Button.Background"]).Color; }
			set { ((SolidColorBrush)this.Resources["Button.Background"]).Color = value; }
		}
		public new Color Foreground {
			get { return ((SolidColorBrush)this.Resources["Button.Foreground"]).Color; }
			set { ((SolidColorBrush)this.Resources["Button.Foreground"]).Color = value; }
		}
		public Color FocusBackground {
			get { return ((SolidColorBrush)this.Resources["Button.MouseOver.Background"]).Color; }
			set { ((SolidColorBrush)this.Resources["Button.MouseOver.Background"]).Color = value; }
		}
		public Color FocusForeground {
			get { return ((SolidColorBrush)this.Resources["Button.MouseOver.Background"]).Color; }
			set { ((SolidColorBrush)this.Resources["Button.MouseOver.Foreground"]).Color = value; }
		}
		public Color PressedBackground {
			get { return ((SolidColorBrush)this.Resources["Button.Pressed.Background"]).Color; }
			set { ((SolidColorBrush)this.Resources["Button.Pressed.Background"]).Color = value; }
		}
		public Color PressedForeground {
			get { return ((SolidColorBrush)this.Resources["Button.Pressed.Foreground"]).Color; }
			set { ((SolidColorBrush)this.Resources["Button.Pressed.Foreground"]).Color = value; }
		}
		public event RoutedEventHandler Click {
			add { AddHandler(ClickEvent, value); }
			remove { RemoveHandler(ClickEvent, value); }
		}
		public CornerRadius CornerRadius { get; set; }

		public new object Content {
			get; set;
		}
		
		private static void PropertiesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
			if (e.Property == BackgroundProperty) {
				((ColoredButton)sender).Background = ((Color)e.NewValue);
			}
			else if (e.Property == ForegroundProperty) {
				((ColoredButton)sender).Foreground = ((Color)e.NewValue);
			}
			else if (e.Property == FocusBackgroundProperty) {
				((ColoredButton)sender).FocusBackground  = ((Color)e.NewValue);
			}
			else if (e.Property == FocusForegroundProperty) {
				((ColoredButton)sender).FocusForeground = ((Color)e.NewValue);
			}
			else if (e.Property == PressedBackgroundProperty) {
				((ColoredButton)sender).PressedBackground = ((Color)e.NewValue);
			}
			else if (e.Property == PressedForegroundProperty) {
				((ColoredButton)sender).PressedBackground = ((Color)e.NewValue);
			}
			else if (e.Property == CornerRadiusProperty) {
				((ColoredButton)sender).CornerRadius = ((CornerRadius)e.NewValue);
			}
			else if (e.Property == ContentProperty) {
				((ColoredButton)sender).Content = (e.NewValue);
			}
		}

		private void MainButton_Click(object sender, RoutedEventArgs e) {
			e.RoutedEvent = ClickEvent;
			this.RaiseEvent(e);
		}
	}
}
